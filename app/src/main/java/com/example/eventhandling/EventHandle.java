package com.example.eventhandling;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;

public class EventHandle extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    String id = "1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_handle);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }
    public void pushUserProfile(Context context){
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID,id);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM,bundle);
    }
    public class ClevertapEvent{

    }
}
