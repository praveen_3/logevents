package com.example.eventhandling.Util

class CleverTapEvent {

    class ProfileProperties{

        companion object {
            var name: String = "Name"
            var rooted: String = "Rooted"
            var email: String = "Email"
            var mobile: String = "Phone"
            var companyId: String = "Company_Id"
            var userId: String = "User_Id"
            var identity: String = "Identity"
            var is_unified: String = "Is_Unified"
            var api_level : String = "Api_Level"
            var version_code : String = "Version_Code"
            var os : String = "Os"
        }
    }

    class PropertiesNames{

        companion object {
            @JvmStatic
            var source: String = "Source"
            @JvmStatic
            var is_scan : String = "Is_Scan"
            @JvmStatic
            var screen_name : String = "Screen_Name"
            @JvmStatic
            var banner_name : String = "Banner_Name"
            @JvmStatic
            var item_name : String = "Item_Name"
            @JvmStatic
            var item_price : String = "Item_Price"
            @JvmStatic
            var item_category : String = "Item_Category"
            @JvmStatic
            var vendor_name : String = "Vendor_Name"
            @JvmStatic
            var vendor_id : String = "Vendor_Id"
            @JvmStatic
            var action : String = "Action"
            @JvmStatic
            var mode : String = "Mode"
            @JvmStatic
            var ocassion_id : String = "Ocassion_ID"
            @JvmStatic
            var cafeteria_name : String = "Cafeteria_name"
            @JvmStatic
            var signin_type : String = "Signin_Type"
            @JvmStatic
            var is_bookmarked : String = "Is_Bookmarked"
            @JvmStatic
            var is_trending : String = "Is_Trending"
            @JvmStatic
            var is_customised : String = "Is_Customised"
            @JvmStatic
            var amount : String = "Amount"
            @JvmStatic
            var location_id : String = "Location_ID"
            @JvmStatic
            var nav_item_name : String = "Nav_Item_Name"
            @JvmStatic
            var previous_location : String = "Previous_Location"
            @JvmStatic
            var new_location : String = "New_Location"
            @JvmStatic
            var is_default : String = "Is_Default"
            @JvmStatic
            var sub_items : String = "Customize_Items"
            @JvmStatic
            var is_change : String = "Is_Change"
            @JvmStatic
            var previous_ocassion : String = "Previous_Ocassion"
            @JvmStatic
            var new_occasion : String = "New_Occasion"
            @JvmStatic
            var user_action : String = "Action"
            @JvmStatic
            var payment_method_name : String = "Payment_Method_Name"
            @JvmStatic
            var rating : String = "Rating"
            @JvmStatic
            var order_status : String = "Order_Status"
            @JvmStatic
            var methods_selected : String = "Methods_Selected"
            @JvmStatic
            var default_method : String = "Default_Method"
            @JvmStatic
            var is_method_change : String = "Is_Method_Change"
            @JvmStatic
            var cart_value : String = "Cart_Value"
            @JvmStatic
            var item_name_item_quantity : String = "Item_Name_Item_Quantity"
            @JvmStatic
            var is_company_paid : String = "Is_Company_Paid"
            @JvmStatic
            var special_instruction : String = "Special_Instruction"
            @JvmStatic
            var setting_name : String = "Setting_Name"
            @JvmStatic
            var campaign_id : String = "Campaign_Id"
            @JvmStatic
            var option_name : String = "Option_Name"
            @JvmStatic
            var answer : String = "Answer"
            @JvmStatic
            var question_id : String = "Question_Id"
            @JvmStatic
            var answer_id : String ="Answer_Id"
            @JvmStatic
            var status : String = "Status"
            @JvmStatic
            var upi_app_name : String ="Upi_App_Name"
            @JvmStatic
            var message : String ="message"
            @JvmStatic
            var reordering : String="reordering"
            @JvmStatic
            var orderId : String="order_id"
        }
    }
}