package com.example.eventhandling.Util;

public interface ActivityConstants {
    String SHARED_PREFERENCES = "event_sh_pref";
    String CLEVERTAP = "clevertap";
    String FIREBASE = "firebase";
    String MULTI_TYPE = "multi_type";
}
