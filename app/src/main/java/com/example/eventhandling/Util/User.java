package com.example.eventhandling.Util;
import java.io.Serializable;
public class User implements Serializable{
    long id;
    @SerializedName("username")
    String email;
    @SerializedName("name")
    String name;
    @SerializedName("mobile_no")
    String phoneNumber;
    @SerializedName("company_id")
    long companyId;
    @SerializedName("email")
    String empEmail = "";
    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getEmpEmail() {
        return empEmail;
    }
    public String getCompanyId() {
        return String.valueOf(companyId);
    }
    public String getPhoneNumber() {
        if (phoneNumber == null)
            return "";
        return phoneNumber;
    }
}
