package com.example.eventhandling;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.clevertap.android.sdk.CleverTapAPI;
import com.example.eventhandling.Util.ActivityConstants;
import com.example.eventhandling.Util.CleverTapEvent;
import com.example.eventhandling.Util.User;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.Map;

public class LogEvent {
    public static FirebaseAnalytics mFirebaseAnalytics;
//    @Override
//    protected void onCreate(Bundle savedInstancestate){
//        super.onCreate(savedInstancestate);
//    }
    public SharedPreferences sharedPreferences;

    public void pushUserEvent(HashMap<String,Object> map,String Eventname, Context context, String isClevertap){
        if(ActivityConstants.CLEVERTAP.equalsIgnoreCase(isClevertap)){
            new Clevertap().pushUserEvents(map,context,Eventname);
        }
        else if(ActivityConstants.FIREBASE.equalsIgnoreCase(isClevertap)){
            FireBase(map, context,Eventname);
        }
    }

    public void pushUserProfile(User user, Context context, String isClevertap){

        if(ActivityConstants.CLEVERTAP.equalsIgnoreCase(isClevertap)){
            new Clevertap().pushUserProfile(user,context);
        }
    }

    private static synchronized FirebaseAnalytics getInstance(Context context){
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        return mFirebaseAnalytics;
    }

    public class Clevertap{
        void pushUserEvents(HashMap<String, Object> map, Context context, String Eventname){
            try {
                CleverTapAPI cleverTapDefaultInstance = CleverTapAPI.getDefaultInstance(context);
                HashMap<String, Object> newmap = new HashMap<>();
                if (map != null) {
                    newmap.putAll(map);
                }
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getName(), sharedPreferences.getString("name", ""));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getEmail(), sharedPreferences.getString("email", ""));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getMobile(), sharedPreferences.getString("phone", ""));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getCompanyId(), sharedPreferences.getString("c_id", ""));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getUserId(), String.valueOf(sharedPreferences.getLong("user_id", 0)));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getIdentity(), sharedPreferences.getLong("identity", 0));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.is_unified(), sharedPreferences.getBoolean("is_unified", false));
                newmap.put(CleverTapEvent.PropertiesNames.getLocation_id(), String.valueOf(sharedPreferences.getLong("location_id", 0)));
                newmap.put(CleverTapEvent.ProfileProperties.Companion.getOs(), "Android");

                if(cleverTapDefaultInstance!=null) {
                    cleverTapDefaultInstance.pushEvent(Eventname, newmap);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        void pushUserProfile(User user,Context context){
            CleverTapAPI cleverTapDefaultInstance = CleverTapAPI.getDefaultInstance(context);
            if(cleverTapDefaultInstance!=null){
                HashMap<String,Object>profileupdate = new HashMap<>();
                profileupdate.put(CleverTapEvent.ProfileProperties.Companion.getName(),user.getName());
                profileupdate.put(CleverTapEvent.ProfileProperties.Companion.getEmail(),user.getEmpEmail());
                profileupdate.put(CleverTapEvent.ProfileProperties.Companion.getMobile(),user.getPhoneNumber());
                profileupdate.put(CleverTapEvent.ProfileProperties.Companion.getCompanyId(),user.getCompanyId());
                profileupdate.put(CleverTapEvent.ProfileProperties.Companion.getUserId(),String.valueOf(user.getId()));
                profileupdate.put(CleverTapEvent.ProfileProperties.Companion.getIdentity(),user.getId());
                cleverTapDefaultInstance.onUserLogin(profileupdate);
            }
        }



        void pushFcmEvents(String fcmId,Context context){
            try {
                CleverTapAPI cleverTapDefaultInstance = CleverTapAPI.getDefaultInstance(context);
                if (cleverTapDefaultInstance != null) {
                    cleverTapDefaultInstance.pushFcmRegistrationId(fcmId, true);
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void FireBase(HashMap<String, Object> map, Context context, String Eventname){
        try {
            getInstance(context);
            Bundle bundle = new Bundle();
            if (map != null) {
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    if (entry.getValue().getClass() == String.class) {
                        bundle.putString(entry.getKey(), (String) entry.getValue());
                    } else if (entry.getValue().getClass() == Boolean.class) {
                        bundle.putBoolean(entry.getKey(), (boolean) entry.getValue());
                    } else if (entry.getValue().getClass() == Double.class) {
                        bundle.putDouble(entry.getKey(), (Double) entry.getValue());
                    } else if (entry.getValue().getClass() == Long.class) {
                        bundle.putLong(entry.getKey(), (Long) entry.getValue());
                    } else if (entry.getValue().getClass() == int.class) {
                        bundle.putInt(entry.getKey(), (int) entry.getValue());
                    }
                }
            }
            bundle.putString(CleverTapEvent.ProfileProperties.Companion.getName(), sharedPreferences.getString("name",""));
            bundle.putString(CleverTapEvent.ProfileProperties.Companion.getEmail(), sharedPreferences.getString("email",""));
            bundle.putString(CleverTapEvent.ProfileProperties.Companion.getMobile(), sharedPreferences.getString("phone",""));
            bundle.putString(CleverTapEvent.ProfileProperties.Companion.getCompanyId(), sharedPreferences.getString("c_id",""));
            bundle.putString(CleverTapEvent.ProfileProperties.Companion.getUserId(), String.valueOf(sharedPreferences.getLong("user_id",0)));
            bundle.putLong(CleverTapEvent.ProfileProperties.Companion.getIdentity(), sharedPreferences.getLong("identity",0));
            bundle.putBoolean(CleverTapEvent.ProfileProperties.Companion.is_unified(), sharedPreferences.getBoolean("is_unified",false));
            bundle.putString(CleverTapEvent.PropertiesNames.getLocation_id(), String.valueOf(sharedPreferences.getLong("location_id",0)));
            bundle.putString(CleverTapEvent.ProfileProperties.Companion.getOs(),"Android");
            /* Todo
            *  check for rooted */
//            try {
//                if (RootBeer(context).isRooted) {
//                    bundle.putBoolean(CleverTapEvent.ProfileProperties.Companion.getRooted(), true);
//                } else {
//                    bundle.putBoolean(CleverTapEvent.ProfileProperties.Companion.getRooted(), false);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            try {
                bundle.putInt(CleverTapEvent.ProfileProperties.Companion.getApi_level(), android.os.Build.VERSION.SDK_INT);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                bundle.putInt(CleverTapEvent.ProfileProperties.Companion.getVersion_code(), BuildConfig.VERSION_CODE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle);
            mFirebaseAnalytics.logEvent(Eventname, bundle);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
